# Lectio to CSV
Gets pupil data from lectio and saves it to a .csv file.
With a QT Gui for ease of use. 

# GDPR stuff
okay so the previous screenshot was from when lectio was public, now it isn't
this resulted in a report to "Datatilsynet", the danish gdpr enforcers.
Even though the emails were fake they were still considered sensitive data.

![Screenshot](screenshot.png)
