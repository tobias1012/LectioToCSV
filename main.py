#/usr/bin/python3

#Import Modules for getting, extracting and modifying the data from lectio.dk
import urllib3
import re
import random
import string
import sys
import csv
import requests
from bs4 import BeautifulSoup

#Import relevant QT bindings. 
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QFileDialog
from PySide2.QtCore import QFile
from PySide2.QtGui import QStandardItemModel
from PySide2.QtGui import QStandardItem

#Function for getting data and puttin in a "QStandardItemModel" 
#which then in turn is used in the QTableView and to export to .csv
def parse(link, suffix, mailDomain, sessionID):
    #Get data from lectio
    #http = urllib3.PoolManager()
    #site = http.request('GET', link)
    site = requests.get(link, cookies={'ASP.NET_SessionId':sessionID})
    soup = BeautifulSoup(site.content, 'html.parser')
    pupils = soup.find(id='s_m_Content_Content_laerereleverpanel_alm_gv')
    fieldnames = ['First name', 'Surname', 'Login Name', 'E-Mail Address', 'Password']
    
    
    #Create container for the data
    TableModel = QStandardItemModel(0,5)
    TableModel.setHorizontalHeaderLabels(fieldnames)

    for index, tr in enumerate(pupils.find_all('tr')[1:]):
        
        #Get raw data from HTML Table
        td = tr.find_all('td')
     
        #Extract and format data
        firstName = re.sub('\\n','',td[2].text)
        surName = td[3].text
        uid = (re.sub('\s','',(re.findall('\w*',firstName)[0]+ ''.join([x for x in re.findall('\s\w', firstName)]))) + ''.join([x for x in re.findall('[_A-Z]',surName)])).lower() + suffix

        if(firstName == "<Navnebeskyttet>" ): #Check if Name protected, generate name with random suffix
            firstName = "Beskyttet"
            surName = "Beskyttet"
            uid = "Beskyttet" + str(random.randint(1000,9999)) + suffix

        passwd = ''.join(random.choice(string.ascii_letters) for i in range(8))
        mail = uid + '@' + mailDomain 
        
        #Set Data in model
        TableModel.setItem(index,0,QStandardItem(firstName))
        TableModel.setItem(index,1,QStandardItem(surName))
        TableModel.setItem(index,2,QStandardItem(uid))
        TableModel.setItem(index,3,QStandardItem(mail))
        TableModel.setItem(index,4,QStandardItem(passwd))


    #Return QStandardItemModel 
    return TableModel

#Function for downloading, parsing and putting the data into the qtableview
def getData():
    global data
    data = parse(window.lineEditAddress.text(), window.lineEditSuffix.text(), window.lineEditMail.text(), window.lineEditSessionID.text())
    window.dataTable.setModel(data)

#Saves data to .csv if there is data to be saved
def saveFile():
    global data
    if 'data' in globals():
        with open(QFileDialog.getSaveFileName(selectedFilter='*.csv')[0]+'.csv', 'w') as file:
            fieldnames = ['givenName', 'sn', 'uid', 'mail', 'password']
            writer = csv.DictWriter(file, fieldnames=fieldnames)

            for x in range (data.rowCount()):
                print(data.data(data.index(x,0)),data.data(data.index(x,1)),data.data(data.index(x,2)),data.data(data.index(x,3)),data.data(data.index(x,4)))
                writer.writerow({'givenName': data.data(data.index(x,0)), 'sn': data.data(data.index(x,1)), 'uid': data.data(data.index(x,2)), 'mail': data.data(data.index(x,3)), 'password': data.data(data.index(x,4))})

#Pyside main function
if __name__ == '__main__':
    app = QApplication(sys.argv)

    file = QFile("mainWindow.ui")
    file.open(QFile.ReadOnly)

    loader = QUiLoader()
    window = loader.load(file)

    window.getDataButton.clicked.connect(getData)
    window.saveToCSVButton.clicked.connect(saveFile)

    window.show()
    sys.exit(app.exec_())
